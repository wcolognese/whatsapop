﻿using WhatsApopCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WhatsApopCommon;

namespace WSWhatsApop
{
    /// <summary>
    /// Summary description for WAService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WAService : System.Web.Services.WebService
    {
        List<Friend> friends = new List<Friend>();

        /*[WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }*/

        [WebMethod]
        public List<Friend> GetFriends()
        {
            //return friends;
            return new List<Friend>()
            {
                new Friend("thayna", "456"),
                new Friend("wagner", "123")
            };
        }

        [WebMethod]
        public void AddFriend(Friend friend)
        {
            friends.Add(friend);
        }
    }
}