﻿namespace WhatApopServer
{
    partial class FormServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSendMsg = new System.Windows.Forms.Button();
            this.lb_msgs = new System.Windows.Forms.ListBox();
            this.lbl_connected = new System.Windows.Forms.Label();
            this.lb_connecteds = new System.Windows.Forms.ListBox();
            this.lbl_msgs = new System.Windows.Forms.Label();
            this.bt_desconnect = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btSendMsg
            // 
            this.btSendMsg.Location = new System.Drawing.Point(209, 227);
            this.btSendMsg.Name = "btSendMsg";
            this.btSendMsg.Size = new System.Drawing.Size(75, 23);
            this.btSendMsg.TabIndex = 0;
            this.btSendMsg.Text = "SendMsg";
            this.btSendMsg.UseVisualStyleBackColor = true;
            this.btSendMsg.Click += new System.EventHandler(this.btSendMsg_Click);
            // 
            // lb_msgs
            // 
            this.lb_msgs.FormattingEnabled = true;
            this.lb_msgs.Location = new System.Drawing.Point(150, 25);
            this.lb_msgs.Name = "lb_msgs";
            this.lb_msgs.Size = new System.Drawing.Size(321, 160);
            this.lb_msgs.TabIndex = 1;
            // 
            // lbl_connected
            // 
            this.lbl_connected.AutoSize = true;
            this.lbl_connected.Location = new System.Drawing.Point(12, 9);
            this.lbl_connected.Name = "lbl_connected";
            this.lbl_connected.Size = new System.Drawing.Size(82, 13);
            this.lbl_connected.TabIndex = 3;
            this.lbl_connected.Text = "IPs Conectados";
            // 
            // lb_connecteds
            // 
            this.lb_connecteds.FormattingEnabled = true;
            this.lb_connecteds.Location = new System.Drawing.Point(15, 25);
            this.lb_connecteds.Name = "lb_connecteds";
            this.lb_connecteds.Size = new System.Drawing.Size(120, 160);
            this.lb_connecteds.TabIndex = 4;
            // 
            // lbl_msgs
            // 
            this.lbl_msgs.AutoSize = true;
            this.lbl_msgs.Location = new System.Drawing.Point(147, 9);
            this.lbl_msgs.Name = "lbl_msgs";
            this.lbl_msgs.Size = new System.Drawing.Size(62, 13);
            this.lbl_msgs.TabIndex = 5;
            this.lbl_msgs.Text = "Mensagens";
            // 
            // bt_desconnect
            // 
            this.bt_desconnect.Location = new System.Drawing.Point(31, 191);
            this.bt_desconnect.Name = "bt_desconnect";
            this.bt_desconnect.Size = new System.Drawing.Size(80, 23);
            this.bt_desconnect.TabIndex = 6;
            this.bt_desconnect.Text = "Desconectar";
            this.bt_desconnect.UseVisualStyleBackColor = true;
            this.bt_desconnect.Click += new System.EventHandler(this.bt_desconnect_Click);
            // 
            // FormServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 262);
            this.Controls.Add(this.bt_desconnect);
            this.Controls.Add(this.lbl_msgs);
            this.Controls.Add(this.lb_connecteds);
            this.Controls.Add(this.lbl_connected);
            this.Controls.Add(this.lb_msgs);
            this.Controls.Add(this.btSendMsg);
            this.Name = "FormServer";
            this.Text = "Server";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSendMsg;
        private System.Windows.Forms.ListBox lb_msgs;
        private System.Windows.Forms.Label lbl_connected;
        private System.Windows.Forms.ListBox lb_connecteds;
        private System.Windows.Forms.Label lbl_msgs;
        private System.Windows.Forms.Button bt_desconnect;
    }
}

