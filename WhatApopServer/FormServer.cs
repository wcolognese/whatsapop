﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WhatsApopCommon;

namespace WhatApopServer
{
    public partial class FormServer : Form
    {
        private const int MAX_LISTENERS = 5;

        static private TcpListener tcpListener;

        static Socket socketForClient;

        private Thread thread;

        static Dictionary<string, Client> Clients = new Dictionary<string, Client>();

        static private ListBox lbMsgs;
        static private ListBox lbConnecteds;

        static WhatsApopCommon.Message Msg;
 
        public FormServer()
        {
            InitializeComponent();

            tcpListener = new TcpListener(MAX_LISTENERS);
            RunServer();

            Msg = new WhatsApopCommon.Message();

            lbMsgs       = lb_msgs;
            lbConnecteds = lb_connecteds;
        }
 
        public void RunServer()
        {
            //Levanta o servidor
            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 2001);
            tcpListener = new TcpListener(ipEndPoint);
            tcpListener.Start();

            for (int i = 0; i < MAX_LISTENERS; i++)
            {
                Thread newThread = new Thread(new ThreadStart(Listeners));
                newThread.Start();
            }

            MessageBox.Show("Servidor habilitado e escutando porta...", "Server App");
        }

        static void Listeners()
        {
            socketForClient = tcpListener.AcceptSocket();
            if (socketForClient.Connected)
            {
                lbConnecteds.Invoke(new MethodInvoker(() => { lbConnecteds.Items.Add(socketForClient.RemoteEndPoint.ToString()); }));

                NetworkStream networkStream = new NetworkStream(socketForClient);
                BinaryReader binaryReader   = new BinaryReader(networkStream);

                Client client = new Client( socketForClient.RemoteEndPoint, 
                                            socketForClient, 
                                            networkStream, 
                                            new BinaryWriter(networkStream),
                                            binaryReader );

                Clients.Add( socketForClient.RemoteEndPoint.ToString() , client );

                WAService.WAServiceSoapClient _client = new WAService.WAServiceSoapClient();
                WAService.Friend[] friends = _client.GetFriends();

                while (true)
                {
                    try
                    {
                        ReceivedMsg(binaryReader.ReadString());
                    }
                    catch
                    {
                        //Offline
                        break;
                    }
                }
            }
        }
 
        private void SendMsg(string ip, string msg)
        {
            try
            {
                Client client;
                if (Clients.TryGetValue(ip, out client))
                    client.binaryWriter.Write(msg);
            }
            catch (SocketException socketEx)
            {
                MessageBox.Show(socketEx.Message, "Erro");
            }
        }

        private void btSendMsg_Click(object sender, EventArgs e)
        {
            if (lbConnecteds.SelectedIndex >= 0)
            {
                SendMsg(lbConnecteds.SelectedItem.ToString(), "aeeew");
            }
        }

        void Connected(string ip)
        {
            if (this.IsHandleCreated)
                lb_connecteds.Invoke(new MethodInvoker(() => { lb_connecteds.Items.Add(ip); }));
        }

        static private void ReceivedMsg(string msg)
        {
            Msg = Msg.BuildReceived(msg);

            lbMsgs.Invoke(new MethodInvoker(() => { lbMsgs.Items.Add("[" + Msg.Sender + "] " + Msg.Msg); }));
        }

        private void bt_desconnect_Click(object sender, EventArgs e)
        {
            if (lbConnecteds.SelectedIndex >= 0)
            {
                if(Disconnect(lbConnecteds.SelectedItem.ToString()));
                lbConnecteds.Items.RemoveAt(lbConnecteds.SelectedIndex);
            }
        }

        static bool Disconnect(string ip)
        {
            Client client;

            if (Clients.TryGetValue(ip, out client))
            {
                client.networkStream.Close();
                client.binaryReader.Close();
                client.binaryWriter.Close();
                client.socket.Close();

                Clients.Remove(ip);
                return true;
            }
            return false;
        }
    }
}
