﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WhatApopServer.WAService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Friend", Namespace="http://tempuri.org/")]
    [System.SerializableAttribute()]
    public partial class Friend : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string userNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IPField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string userName {
            get {
                return this.userNameField;
            }
            set {
                if ((object.ReferenceEquals(this.userNameField, value) != true)) {
                    this.userNameField = value;
                    this.RaisePropertyChanged("userName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string IP {
            get {
                return this.IPField;
            }
            set {
                if ((object.ReferenceEquals(this.IPField, value) != true)) {
                    this.IPField = value;
                    this.RaisePropertyChanged("IP");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="WAService.WAServiceSoap")]
    public interface WAServiceSoap {
        
        // CODEGEN: Generating message contract since element name GetFriendsResult from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetFriends", ReplyAction="*")]
        WhatApopServer.WAService.GetFriendsResponse GetFriends(WhatApopServer.WAService.GetFriendsRequest request);
        
        // CODEGEN: Generating message contract since element name friend from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/AddFriend", ReplyAction="*")]
        WhatApopServer.WAService.AddFriendResponse AddFriend(WhatApopServer.WAService.AddFriendRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetFriendsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetFriends", Namespace="http://tempuri.org/", Order=0)]
        public WhatApopServer.WAService.GetFriendsRequestBody Body;
        
        public GetFriendsRequest() {
        }
        
        public GetFriendsRequest(WhatApopServer.WAService.GetFriendsRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class GetFriendsRequestBody {
        
        public GetFriendsRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetFriendsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetFriendsResponse", Namespace="http://tempuri.org/", Order=0)]
        public WhatApopServer.WAService.GetFriendsResponseBody Body;
        
        public GetFriendsResponse() {
        }
        
        public GetFriendsResponse(WhatApopServer.WAService.GetFriendsResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetFriendsResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public WhatApopServer.WAService.Friend[] GetFriendsResult;
        
        public GetFriendsResponseBody() {
        }
        
        public GetFriendsResponseBody(WhatApopServer.WAService.Friend[] GetFriendsResult) {
            this.GetFriendsResult = GetFriendsResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class AddFriendRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="AddFriend", Namespace="http://tempuri.org/", Order=0)]
        public WhatApopServer.WAService.AddFriendRequestBody Body;
        
        public AddFriendRequest() {
        }
        
        public AddFriendRequest(WhatApopServer.WAService.AddFriendRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class AddFriendRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public WhatApopServer.WAService.Friend friend;
        
        public AddFriendRequestBody() {
        }
        
        public AddFriendRequestBody(WhatApopServer.WAService.Friend friend) {
            this.friend = friend;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class AddFriendResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="AddFriendResponse", Namespace="http://tempuri.org/", Order=0)]
        public WhatApopServer.WAService.AddFriendResponseBody Body;
        
        public AddFriendResponse() {
        }
        
        public AddFriendResponse(WhatApopServer.WAService.AddFriendResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class AddFriendResponseBody {
        
        public AddFriendResponseBody() {
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WAServiceSoapChannel : WhatApopServer.WAService.WAServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WAServiceSoapClient : System.ServiceModel.ClientBase<WhatApopServer.WAService.WAServiceSoap>, WhatApopServer.WAService.WAServiceSoap {
        
        public WAServiceSoapClient() {
        }
        
        public WAServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WAServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WAServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WAServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        WhatApopServer.WAService.GetFriendsResponse WhatApopServer.WAService.WAServiceSoap.GetFriends(WhatApopServer.WAService.GetFriendsRequest request) {
            return base.Channel.GetFriends(request);
        }
        
        public WhatApopServer.WAService.Friend[] GetFriends() {
            WhatApopServer.WAService.GetFriendsRequest inValue = new WhatApopServer.WAService.GetFriendsRequest();
            inValue.Body = new WhatApopServer.WAService.GetFriendsRequestBody();
            WhatApopServer.WAService.GetFriendsResponse retVal = ((WhatApopServer.WAService.WAServiceSoap)(this)).GetFriends(inValue);
            return retVal.Body.GetFriendsResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        WhatApopServer.WAService.AddFriendResponse WhatApopServer.WAService.WAServiceSoap.AddFriend(WhatApopServer.WAService.AddFriendRequest request) {
            return base.Channel.AddFriend(request);
        }
        
        public void AddFriend(WhatApopServer.WAService.Friend friend) {
            WhatApopServer.WAService.AddFriendRequest inValue = new WhatApopServer.WAService.AddFriendRequest();
            inValue.Body = new WhatApopServer.WAService.AddFriendRequestBody();
            inValue.Body.friend = friend;
            WhatApopServer.WAService.AddFriendResponse retVal = ((WhatApopServer.WAService.WAServiceSoap)(this)).AddFriend(inValue);
        }
    }
}
