﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace WhatsApopCommon
{
    public class Client
    {
        public EndPoint IP { get; set; }
        public Socket socket { get; set; }
        public NetworkStream networkStream { get; set; }
        public BinaryWriter binaryWriter { get; set; }
        public BinaryReader binaryReader { get; set; }

        public Client(EndPoint IP, Socket socket, NetworkStream networkStream, BinaryWriter binaryWriter, BinaryReader binaryReader)
        {
            this.IP             = IP;
            this.socket         = socket;
            this.networkStream  = networkStream;
            this.binaryWriter   = binaryWriter;
            this.binaryReader   = binaryReader;
        }
    }
}
