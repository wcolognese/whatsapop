﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhatsApopCommon
{
    [Serializable]
    public class Friend
    {
        public string userName { get; set; }
        public string IP { get; set; }

        public Friend(string userName, string IP)
        {
            this.userName = userName;
            this.IP         = IP;
        }

        public Friend()
        { }
    }
}
