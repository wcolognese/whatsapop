﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WhatsApopCommon
{
    [Serializable]
    public class Message
    {
        public string Sender { get; set; }
        public string destinyRoom { get; set; }
        public string Msg { get; set; }

        public string BuildToSend(string Sender, string destinyRoom, string Msg)
        {
            this.Sender = Sender;
            this.destinyRoom = destinyRoom;
            this.Msg = Msg;

            return GetXMLFromObject(this);
        }

        /// <summary>
        /// Constroi a mensagem recebida em um objeto mensagem.
        /// </summary>
        /// <param name="message"></param>
        /// <returns>Message</returns>
        public Message BuildReceived(string message)
        {
            return (Message)ObjectToXML(message, this.GetType());
        }

        private string GetXMLFromObject(object o) 
        {
            XmlSerializer serializer = new XmlSerializer(o.GetType());
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = new XmlTextWriter(sw);
            serializer.Serialize(tw, o);
            return sw.ToString();
        }

        private Object ObjectToXML(string XmlOfAnObject, Type ObjectType)
        {
            StringReader StrReader = new StringReader(XmlOfAnObject);
            XmlSerializer serializer = new XmlSerializer(ObjectType);
            XmlTextReader XmlReader = new XmlTextReader(StrReader);
     
            Object AnObject = serializer.Deserialize(XmlReader);
            return AnObject;
        }
    }
}
