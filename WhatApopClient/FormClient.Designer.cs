﻿namespace WhatApopClient
{
    partial class FormClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSendMsg = new System.Windows.Forms.Button();
            this.txt_Dialog = new System.Windows.Forms.TextBox();
            this.txt_Message = new System.Windows.Forms.TextBox();
            this.lb_friends = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btSendMsg
            // 
            this.btSendMsg.Location = new System.Drawing.Point(671, 345);
            this.btSendMsg.Name = "btSendMsg";
            this.btSendMsg.Size = new System.Drawing.Size(80, 50);
            this.btSendMsg.TabIndex = 0;
            this.btSendMsg.Text = "SendMsg";
            this.btSendMsg.UseVisualStyleBackColor = true;
            this.btSendMsg.Click += new System.EventHandler(this.btSendMsg_Click);
            // 
            // txt_Dialog
            // 
            this.txt_Dialog.Location = new System.Drawing.Point(152, 21);
            this.txt_Dialog.Multiline = true;
            this.txt_Dialog.Name = "txt_Dialog";
            this.txt_Dialog.Size = new System.Drawing.Size(599, 309);
            this.txt_Dialog.TabIndex = 1;
            // 
            // txt_Message
            // 
            this.txt_Message.Location = new System.Drawing.Point(152, 336);
            this.txt_Message.Multiline = true;
            this.txt_Message.Name = "txt_Message";
            this.txt_Message.Size = new System.Drawing.Size(503, 59);
            this.txt_Message.TabIndex = 2;
            // 
            // lb_friends
            // 
            this.lb_friends.FormattingEnabled = true;
            this.lb_friends.Location = new System.Drawing.Point(12, 21);
            this.lb_friends.Name = "lb_friends";
            this.lb_friends.Size = new System.Drawing.Size(120, 303);
            this.lb_friends.TabIndex = 3;
            // 
            // FormClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 398);
            this.Controls.Add(this.lb_friends);
            this.Controls.Add(this.txt_Message);
            this.Controls.Add(this.txt_Dialog);
            this.Controls.Add(this.btSendMsg);
            this.Name = "FormClient";
            this.Text = "Client";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSendMsg;
        private System.Windows.Forms.TextBox txt_Dialog;
        private System.Windows.Forms.TextBox txt_Message;
        private System.Windows.Forms.ListBox lb_friends;
    }
}

