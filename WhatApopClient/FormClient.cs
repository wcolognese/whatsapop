﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace WhatApopClient
{
    public partial class FormClient : Form
    {
        private NetworkStream networkStream;
        private BinaryWriter binaryWriter;
        private BinaryReader binaryReader;

        private TcpClient tcpClient;
 
        private Thread thread;

        WhatsApopCommon.Message Msg;
        static string userName;
 
        public FormClient(string name) 
        {
            InitializeComponent();
            thread = new Thread(new ThreadStart(RunClient));
            thread.Start();

            Msg = new WhatsApopCommon.Message();
            userName = name;
        }
 
        public void RunClient() 
        {
            try 
            {
                tcpClient = new TcpClient();
                tcpClient.Connect("127.0.0.1", 2001);
 
                networkStream = tcpClient.GetStream();

                binaryReader = new BinaryReader(networkStream);
                binaryWriter = new BinaryWriter(networkStream);
               
                while(true) 
                {
                    try 
                    {
                        ReceivedMsg( binaryReader.ReadString() );
                    } 
                    catch (Exception ex) 
                    {
                        MessageBox.Show(ex.Message, "Erro");
                        break;
                    }
                }
 
                binaryWriter.Close();
                binaryReader.Close();
                networkStream.Close();
                tcpClient.Close();
            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro");
            }
        }
 
        private void btSendMsg_Click(object sender, EventArgs e) 
        {
            if (txt_Message.Text.Length > 0)
            {
                SendMsg(txt_Message.Text);
            }
        }

        private void SendMsg(string msg)
        {
            try
            {
                binaryWriter.Write(Msg.BuildToSend(userName, "123", msg));
                binaryWriter.Flush();
            }
            catch (SocketException socketEx)
            {
                MessageBox.Show(socketEx.Message, "Erro");
            }
            catch (ObjectDisposedException objDisposed)
            {
                MessageBox.Show("Você está desconectado!");
            }
        }

        private void ReceivedMsg(string msg)
        {
            txt_Dialog.Invoke(new MethodInvoker(() => { txt_Dialog.AppendText( msg + "\r\n"); }));
        }
    }
}
